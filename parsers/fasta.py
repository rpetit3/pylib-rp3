'''
    Fasta Parser
    Robert A Petit III
    12/9/2013
    
    Parses a input FASTA file.  It assumes the input follows a standard FASTA 
    format. By default it reads as text, but you can supply a gzip compressed
    FASTA file.
    
    >Simulated Sequence
    ACGTACTGACTGACTGACTGCTAGCATGCTGCATGCTAGCTAGCTCGTACGTGTACTGCTGCATGCATGCTGCAG
    ATAGCTAGCTAGCTACGTAGCTACGTACGTACGTACGTACGTACGTACGGACGCGCGGCGCGATTACGTAGCAGC
    CATGCATGCGCTAGCTAATTA
    
    Example usage:
    from parsers.fasta import Fasta
    Fasta.reader( your_fasta_file )
    
    Fasta.records
    Fasta.sequences
    
'''
class Fasta( object ):
    
    def __init__(self):
        self.records = []
        self.sequences = {}
        self.__fasta = False
        
    def __open_file(self, fasta, gz=False):
        try:
            if gz:
                import gzip
                self.__fasta = gzip.open( fasta )
            else:
                self.__fasta = file( fasta )
        except IOError:                     
            print "Unable to open %s, please check it exists." % fasta
            return
    
    def parse(self, fasta, gz=False, is_file=True):
        if is_file:
            self.__open_file(fasta, gz)
        else:
            self.__fasta = fasta
        
        id = None
        seq = []
        for line in self.__fasta:
            if line.startswith('>'):
                if (len(seq) > 0):
                    self.records.append( id )
                    self.sequences[ id ] = ''.join(seq)
                    seq = []
                    
                id = line[1:].rstrip('\n')
            else:
                seq.append(line.rstrip('\n'))

        self.records.append( id )
        self.sequences[ id ] = ''.join(seq)

        return len(self.sequences)