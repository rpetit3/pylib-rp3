'''
    FASTQ Parser
    Robert A. Petit III
    12/13/2013
    
    Parses an input FASTQ file.  It assumes the FASTQ file is in standard four 
    line format, but has the option to read over multiple lines.
    
    @Simulated Sequence
    ACGTACTGACTGACTGACTGCTAGCATGCTGCATGCTAGCTAGCTCGTACGTGTACTGCTGCATGCATGCTG...
    +Simulated Sequence
    !''*((((***+))%%%++)(%%%%).1***-+*''))**55CCF>>>>>>CCCC+*''))**55CCF>>CC...
    
    Example usage:
    from parsers.fastq import Fastq
    Fastq.reader( your_fastq_file )
    
    Fastq.records
    Fastq.sequences
    Fastq.qualities
    
'''
class Fastq( object ):
    
    def __init__(self):
        self.records = []
        self.sequences = {}
        self.qualities = {}
        
    def reader(self, fasta, gz=False):
        try:
            if gz:
                import gzip
                fastq = gzip.open( fasta )
            else:
                fastq = file( fasta )
        except IOError:                     
            print "Unable to open %s, please check it exists." % fasta
            return
        
        while 1:
            record = fastq.readline()
            if not head:
                break

            self.sequences[ record ] = fastq.readline().rstrip()
            fastq.readline()
            self.qualities[ record ] = fastq.readline().rstrip()
    
