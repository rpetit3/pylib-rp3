'''
    SNP related tools
    Robert A Petit III
    12/11/2013
'''
class Tools( object ):

    '''
        extract_polymorphism( dict, string )
        
        Returns a frequency distribution of polymorphic sites.
        
        Input should be a dictionary of aligned sequences.  These sequences
        should not contain any gaps. Also the reference name should be given 
        to compare each sequence against.
        
        Example:
        >>> seq = {'Seq001':'ATGC', 'Seq002':'AAGG', 'Seq003':'ATGT'}
        >>> extract_polymorphism( seq, 'Seq001')
        2   T   A   0.3333
        4   C   G/T 0.6666
    '''
    def extract_polymorphism( self, seq, reference ):
        ref = seq[ reference ]
        seq = [ v for k,v in seq.items() if k != reference ]

        for i in xrange(len(ref)):
            poly = {}
            count = 0
            for j in xrange(len(seq)):
                if seq[j][i] != ref[i]:
                    count += 1.0
                    poly[seq[j][i]] = 1

            if count > 0:
                print '%s\t%s\t%s\t%.4f' % (i+1, ref[i], '/'.join(sorted(poly.keys())), count/len(seq))
            