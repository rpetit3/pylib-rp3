#! /usr/bin/env python
"""
     Author: Robert A Petit III
       Date: 3/27/2014
    
    Read MSA FASTA and determine if the sequences are identical
    
    Options:
      -f|--fasta STRING         Input ungapped MSA FASTA file
      -g|--gzip                 Input FASTA is compressed using GZIP
      -h, --help                Show this help message and exit
      
    Example: ./find_identical_sequences.py --fasta=data/test_ungapped_msa.fa.gz
"""

if __name__ == '__main__':
    import os
    import sys
    import argparse as ap
    import numpy
    from parsers import fasta

    parser = ap.ArgumentParser(prog='ungapped_msa_to_hap.py', 
                               conflict_handler='resolve', 
                               description="Convert ungapped MSA to HAP.")
    group1 = parser.add_argument_group('Options', '')
    group1.add_argument('-f', '--fasta', required=True, 
                        help='Input ungapped MSA FASTA file', metavar="STRING")
    group1.add_argument('-g', '--gzip', action="store_true",
                        help='Input FASTA is compressed using GZIP')
    group1.add_argument('-h', '--help', action='help', 
                        help='Show this help message and exit')

    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    if not os.path.isfile( args.fasta ):
        print "Please check if input '%s' exists, then try again." % args.fasta
        sys.exit(1)

    # Read FASTA file
    fasta = fasta.Fasta()
    seq_count = fasta.parse( args.fasta, gz=args.gzip )

    seq = numpy.array([ len(v) for k,v in fasta.sequences.items() ])

    if seq.min() == seq.max():
        snp_pos = []
        for pos in xrange(0,seq.max()):
            test_base = fasta.sequences[fasta.records[0]][pos]
            for record in fasta.records:
                if fasta.sequences[record][pos] != test_base:
                    snp_pos.append(pos)
                    break
        if len(snp_pos) > 0:
            print 'cp {0} diff/{1}'.format(args.fasta, args.fasta)
        else:
            print 'All sequences are identical, nothing to output.'
    else:
        print 'Sequence lengths are not equal, exiting.'
