#! /usr/bin/env python
"""
     Author: Robert A Petit III
       Date: 3/27/2014
    
    Given a a file of GI retrieve the corresponding FASTA file from 
    the NCBI.
    
    Options:
      -g|--gi STRING     Input list of gi file
      -h, --help         Show this help message and exit
      
    Example: ./fetch_fasta.py --gi=data/sa_resistome_nucleotide.txt
"""
if __name__ == '__main__':
    import os
    import sys
    import argparse as ap
    from databases import ncbi

    parser = ap.ArgumentParser(prog='fetch_proteins.py', 
                               conflict_handler='resolve', 
                               description="Fetch FASTA files from NCBI.")
    group1 = parser.add_argument_group('Options', '')
    group1.add_argument('-g', '--gi', required=True, 
                        help='Input gi file', metavar="STRING")
    group1.add_argument('-h', '--help', action='help', 
                        help='Show this help message and exit')
                                     
    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    if not os.path.isfile(args.gi):
        print "Please verify '%s' exists, then try again." % args.gi
        sys.exit(1)

    # Read gi file
    fh = open(args.gi, "r")
    for line in fh:
        if not line.startswith('#'):
            gene, gi = line.rstrip().split()
            eutils = ncbi.EUtils('protein', 1, gi)
            eutils.efetch(out='./', name=gene, ext='faa')
            eutils.elink('protein', gi, 'protein_nucleotide')
            eutils.db = 'nucleotide'
            eutils.efetch(out='./', name=gene, ext='fna')
            print '{0}\t{1}\t{2}'.format(gene, gi, eutils.uids[0])
    fh.close()
