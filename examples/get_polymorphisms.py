#! /usr/bin/env python
"""
     Author: Robert A Petit III
       Date: 12/11/2013
    
    Given a FASTA file of aligned sequences (without gaps) a table of 
    polymorphisms and their frequency is produced.
    
    Options:
      -f|--fasta STRING         Input compressed FASTA file
      -r|--reference STRING     Output directory
      -h, --help                Show this help message and exit
      
    Example: ./get_polymorphisms.py --fasta=data/test_protein.fa.gz -r='M3782'
"""

if __name__ == '__main__':
    import os
    import sys
    import argparse as ap
    from parsers import fasta
    from tools import snp

    parser = ap.ArgumentParser(prog='get_polymorphism.py', 
                               conflict_handler='resolve', 
                               description="Frequency table of polymorphisms.")
    group1 = parser.add_argument_group('Options', '')
    group1.add_argument('-f', '--fasta', required=True, 
                        help='Input FASTA file', metavar="STRING")
    group1.add_argument('-r', '--reference', required=True, 
                        help='Reference name', metavar="STRING")
    group1.add_argument('-h', '--help', action='help', 
                        help='Show this help message and exit')
                                     
    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    if not os.path.isfile( args.fasta ):
        print "Please check if input '%s' exists, then try again." % args.fasta
        sys.exit(1)

    # Read FASTA file
    Fasta = fasta.Fasta()
    seq_count = Fasta.reader( args.fasta, gz=True )

    if args.reference in Fasta.sequences:
        # Get polymorphisms from sequences
        Tools = snp.Tools()
        Tools.extract_polymorphism( Fasta.sequences, args.reference )
    else:
        print '%s record not found in input FASTA file.' % args.reference
