#! /usr/bin/env python
"""
     Author: Robert A Petit III
       Date: 2/24/2014
    
    Given a a file of locus tags retrieve the corresponding protein FASTA file 
    from the NCBI.
    
    Options:
      -l|--locus_tag STRING     Input list of locus tags file
      -h, --help                Show this help message and exit
      
    Example: ./search_and_fetch_fasta.py --locus_tag=data/sa_locus_tags.txt
"""

if __name__ == '__main__':
    import os
    import sys
    import argparse as ap
    from databases import ncbi

    parser = ap.ArgumentParser(prog='search_and_fetch_fasta.py', 
                               conflict_handler='resolve', 
                               description="Fetch FASTA files from NCBI.")
    group1 = parser.add_argument_group('Options', '')
    group1.add_argument('-l', '--locus_tag', required=True, 
                        help='Input locus tag file', metavar="STRING")
    group1.add_argument('-h', '--help', action='help', 
                        help='Show this help message and exit')
                        
    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    if not os.path.isfile(args.locus_tag):
        print "Please verify '%s' exists, then try again." % args.locus_tag
        sys.exit(1)

    # Read locus tag file
    fh = open(args.locus_tag, "r")
    for line in fh:
        if not line.startswith('#'):
            locus_tag = line.rstrip()
            eutils = ncbi.EUtils('gene', 1, locus_tag)
            eutils.esearch(locus_tag)
            gi = eutils.uids[0]
            if not gi:
                continue
            eutils.elink('gene', gi, 'gene_protein_refseq')
            eutils.db = 'protein'
            eutils.efetch(out='./', name=locus_tag, ext='faa')
            print '{0}\t{1}\t{2}'.format(locus_tag, gi, eutils.uids[0])
    fh.close()
