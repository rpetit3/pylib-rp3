#! /usr/bin/env python
"""
     Author: Robert A Petit III
       Date: 2/19/2014
    
    Given a FASTA file of aligned sequences, output the min, mean, median, and 
    max read length.
    
    Options:
      -f|--fasta STRING         Input FASTA file
      -g|--gzip                 Input FASTA is compressed using GZIP
      -h, --help                Show this help message and exit
      
    Example: ./get_read_lengths.py --fasta=data/test_protein.fa.gz
"""

if __name__ == '__main__':
    import os
    import sys
    import argparse as ap
    import numpy
    from parsers import fasta

    parser = ap.ArgumentParser(prog='get_read_lengths.py', 
                               conflict_handler='resolve', 
                               description="Summary of FASTA read lengths.")
    group1 = parser.add_argument_group('Options', '')
    group1.add_argument('-f', '--fasta', required=True, 
                        help='Input FASTA file', metavar="STRING")
    group1.add_argument('-g', '--gzip', action="store_true",
                        help='Input FASTA is compressed using GZIP')
    group1.add_argument('-h', '--help', action='help', 
                        help='Show this help message and exit')
                                     
    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(1)

    args = parser.parse_args()  
    if not os.path.isfile( args.fasta ):
        print "Please check if input '%s' exists, then try again." % args.fasta
        sys.exit(1)

    # Read FASTA file
    fasta = fasta.Fasta()
    seq_count = fasta.reader( args.fasta, gz=args.gzip )

    seq = numpy.array([ len(v) for k,v in fasta.sequences.items() ])
    print '%s\t%d\t%.2f\t%.2f\t%d\t%s' % (args.fasta, seq.min(), seq.mean(), 
                                      numpy.median(seq), seq.max(), 
                                      'EQUAL' if seq.min() == seq.max() else 'UNEQUAL')
