"""


"""
import os
from Bio import Entrez
Entrez.email = 'robert.petit@emory.edu'

class EUtils( object ):
    
    def __init__( self, db, retmax, gi):
        self.db = db
        self.retmax = retmax
        self.uids = [gi] 
        
    def esearch( self, term ):
        handle = Entrez.esearch(db=self.db, retmax=self.retmax, term=term)
        record = Entrez.read( handle )
        self.uids = record["IdList"]
        return record["Count"]

    def elink(self, dbfrom, id, linkname):
        handle = Entrez.elink(dbfrom=dbfrom, id=id, linkname=linkname)
        record = Entrez.read(handle)
        self.uids = [record[0]["LinkSetDb"][0]["Link"][0]["Id"]]
        
    def efetch( self, out='.', rettype='fasta', retmode='text', name=False, 
                ext='fa'):
        info_fh = open( out + '/info.out', "w")
        for id in self.uids:
            handle = Entrez.esummary(db=self.db, id=id)
            record = Entrez.read(handle)
            if not name:
                name = record[0]["Id"]
            fasta = '{0}/{1}.{2}'.format(out, name, ext)
            info_fh.write(record[0]["Caption"] +'\t'+ record[0]["Title"] +'\n')
            if not os.path.isfile( fasta ):
                #print '{0}\t{1}'.format(record[0]["Id"], record[0]["Caption"])
                efetch = Entrez.efetch(db=self.db, id=id, rettype=rettype, 
                                      retmode=retmode)
                fasta_fh = open( fasta, "w")
                fasta_fh.write( efetch.read() )
                fasta_fh.close()
                efetch.close()
        info_fh.close()
        
